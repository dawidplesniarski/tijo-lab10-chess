package pl.edu.pwsztar.service.impl;

import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.service.MoveService;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
public class MoveServiceImpl implements MoveService {

    @Override
    public Boolean bishopMove(FigureMoveDto figureMoveDto) {
        int currentRow;
        int newRow;
        int currentCol;
        int newCol;
        // rozdzielamy string na 2 zmienne: wiersz i kolumna
        List<String> startPoint = Arrays.asList(figureMoveDto.getStart().split("_"));
        List<String> destinationPoint = Arrays.asList(figureMoveDto.getDestination().split("_"));

        //przypisujemy zmiennym wartości
        currentCol =  mapToInt(startPoint.get(0));
        currentRow = Integer.parseInt(startPoint.get(1));
        newCol = mapToInt(destinationPoint.get(0));
        newRow = Integer.parseInt(destinationPoint.get(1));
        return Math.abs((currentRow - newRow)) == Math.abs(currentCol - newCol);
    }

    private Integer mapToInt(String key){
        Map<String,Integer> columns = new LinkedHashMap<>();
        columns.put("a",1);
        columns.put("b",2);
        columns.put("c",3);
        columns.put("d",4);
        columns.put("e",5);
        columns.put("f",6);
        columns.put("g",7);
        columns.put("h",8);

        return columns.get(key);
    }
}
